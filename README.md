# Control Arduino con servidor Node.js y página Web

El control de la placa Arduino-Leonardo, concretamente de los pines 8, 9, 10 y 11. Se realiza a través del servidor *Node.js*, además de los framework *Johnny-Five* y *Socket.IO*.

En el Arduino se debe usar el *Firmata* disponible en el IDE, concretamente usar el *StandarFirmata*.

## Firmata

Es el protocolo que permite comunicar los uC con los dispositivos usando distintos lenguajes de programación.

Entorno IDE de Arduino:

**Archivo > Ejemplos > Firmata > StandarFirmata**

Seleccionar y cargar en la placa Arduino.

## NodeJS

La versión de Node utilizada es la *v18.15.0* y la de npm es la *v9.5.0*

## Socket.IO

Es la librería que permite la comunicación bidireccional, con baja latencia y basado en eventos, entre cliente y servidor.

La instalación se realiza a través del package.json, siendo la versión utilizada la *v4.6.1*.

## Express

Framework de comunicación, versión utilizada *v4.18.2*.

## Johnny-Five

Framework que permite el control IoT, basado en JavaScript. Facilitando la comunicación entre la aplicación y la placa Arduino, el Firmata.

La versión utilizada es la *v2.1.0*.

### Key

#### index.js

Explicación de líneas de código del servidor.

> var board = new Board("/dev/ttyACM0"); //se puede omitir la dirección del puerto.

> app.get('/', (req, res) => {
>> es.sendFile(__dirname + '/index.html');
>>> //el archivo de la página web está en la carpeta raiz del proyecto.
> });

> io.on('connection', (socket) => {
>> socket.on('azul', () => {  
>>> //la palabra 'azul' debe cerresponder con la indicada en JS de la página web (cliente)                                
>> led_8.on();
>> });
> });

### index.html

Explicación de líneas de código de la página web (cliente)

> script src="/socket.io/socket.io.js"/script
> script src="index.js"/script
>> // Referencias hacia el servidor socket local y hacia el servidor del código escrito para este proyecto 
> script src="https://cdn.socket.io/4.6.1/socket.io.js"/script
>> // ESta línea puede sustituir al servidor local 

> function cambiaAzul(){
>> var socket_1 = io();            
>> socket_1.emit('azul');                                   
> }
>> //función JS dentro del HTML que se activa al pulsar el button.

> button id="b_azul" type="button" class="btn btn-lg btn-primary" onclick="cambiaAzul()" ON PIN 8/button
>> //Evento onclick que accede a la función JS.

> node index.js 
>> //para iniciar el servidor
> http://localhost:3000 
>> //para visualizar interfaz de control en el navegador

## Interfaz Gráfica

![Interfaz gráfica](pagwebcontard_01.png)

## Anexo

* <http://johnny-five.io/>
* <https://socket.io/>
* <https://nodejs.org/en>
* <https://expressjs.com/>
