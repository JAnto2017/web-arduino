const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
//------------------------------------------------
const {Board,Led} = require('johnny-five');
//------------------------------------------------
var board = new Board("/dev/ttyACM0");
//------------------------------------------------
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});
//------------------------------------------------
board.on('ready',() =>{
    const led_8 = new Led(8);
    const led_9 = new Led(9);
    const led_10 = new Led(10);
    const led_11 = new Led(11);
    io.on('connection', (socket) => {
        socket.on('azul', () => {                                  
            led_8.on();
        });
        socket.on('verde', () => {            
            led_9.on();
        });
        socket.on('rojo', () => {            
            led_10.on();                       
        });
        socket.on('onBlink', () => {                               
            led_11.blink(500);            
        });
        socket.on('stopAzul', () => {                        
            led_8.off();            
        });
        socket.on('stopVerde', () => {                       
            led_9.off();            
        });
        socket.on('stopRojo', () => {                        
            led_10.off();            
        });
        socket.on('offBlink', () => {
            led_11.stop();            
        });
    });
});
//------------------------------------------------
server.listen(3000, () => {
    console.log('listening on *:3000');
});
//------------------------------------------------
/*
io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', () => {
        console.log('user disconnected');
      });
});
*/
//------------------------------------------------
